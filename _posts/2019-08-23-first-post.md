---
title: "First Post"
date: 2019-08-23 14:43:28 -0400
categories: home
---

Now is the time for all good men to come to
the aid of their country. This is just a
regular paragraph.

The quick brown fox jumped over the lazy
dog's back.

### Header 3

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> ## This is an H2 in a blockquote

***

Some of these words *are emphasized*.
Some of these words _are emphasized also_.

Use two asterisks for **strong emphasis**.
Or, if you prefer, __use two underscores instead__.
  
***

This is an [example link](http://example.com/).

***

I get 10 times more traffic from [Google][1] than from
[Yahoo][2] or [MSN][3].

[1]: http://google.com/        "Google"
[2]: http://search.yahoo.com/  "Yahoo Search"
[3]: http://search.msn.com/    "MSN Search"

***

I start my morning with a cup of coffee and
[The New York Times][NY Times].

[ny times]: http://www.nytimes.com/

***

![alt text](https://koreacentral1-mediap.svc.ms/transform/thumbnail?provider=spo&inputFormat=png&cs=fFNQTw&docid=https%3A%2F%2Fcodebluekr-my.sharepoint.com%3A443%2F_api%2Fv2.0%2Fdrives%2Fb!uMT-SpXuiU-XO2wQzp6VaumfZuwjmOtLsQ71HIzB97l0ZBTPCWE5QIICFPo9zfhP%2Fitems%2F01CI7F76Q4SCUJXXN25NALVBRIUD5TF23H%3Fversion%3DPublished&access_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJub25lIn0.eyJhdWQiOiIwMDAwMDAwMy0wMDAwLTBmZjEtY2UwMC0wMDAwMDAwMDAwMDAvY29kZWJsdWVrci1teS5zaGFyZXBvaW50LmNvbUAyN2M0OGE1Yy0zM2JkLTQyZWQtYTk1Yi04NmZmZWY3OTRiMjIiLCJpc3MiOiIwMDAwMDAwMy0wMDAwLTBmZjEtY2UwMC0wMDAwMDAwMDAwMDAiLCJuYmYiOiIxNTY2NTQxMjYwIiwiZXhwIjoiMTU2NjU2Mjg2MCIsImVuZHBvaW50dXJsIjoiUmRJelI0K2FvSUlOZVc4OVUrQWNoSlRoTHBEcUwzNDY4TXdPSVB6aUo2OD0iLCJlbmRwb2ludHVybExlbmd0aCI6IjEyMCIsImlzbG9vcGJhY2siOiJUcnVlIiwiY2lkIjoiWmpkbU5HWmpPV1V0T0RCbU15MDVNREF3TFdabU1tRXRPR1V5TUdJeE56VmlZMlkxIiwidmVyIjoiaGFzaGVkcHJvb2Z0b2tlbiIsInNpdGVpZCI6Ik5HRm1aV00wWWpndFpXVTVOUzAwWmpnNUxUazNNMkl0Tm1NeE1HTmxPV1U1TlRaaCIsIm5hbWVpZCI6IjAjLmZ8bWVtYmVyc2hpcHx1cm4lM2FzcG8lM2Fhbm9uIzRkNDEwZDRiMWQ1ZDVkN2FkZGNlMzhkNGEwZmIyMzg2NGVjMzI4ZTAwMzViYWI3ZDVlMWRiMDcxMTQ3NjI0MjEiLCJuaWkiOiJtaWNyb3NvZnQuc2hhcmVwb2ludCIsImlzdXNlciI6InRydWUiLCJjYWNoZWtleSI6IjBoLmZ8bWVtYmVyc2hpcHx1cm4lM2FzcG8lM2Fhbm9uIzRkNDEwZDRiMWQ1ZDVkN2FkZGNlMzhkNGEwZmIyMzg2NGVjMzI4ZTAwMzViYWI3ZDVlMWRiMDcxMTQ3NjI0MjEiLCJzaGFyaW5naWQiOiJJTmN3bisrT2hrbVN6NlFGRGFYTW9RIiwidHQiOiIwIiwidXNlUGVyc2lzdGVudENvb2tpZSI6IjIifQ.ZWlEOXcybzJaYnB1TEtya1RGTUR4M050clZNMmJqZ2gxeUZtZ2NyZWRKdz0&encodeFailures=1&srcWidth=&srcHeight=&width=199&height=198&action=Access "codeblue")

***

I strongly recommend against using any `<blink>` tags.

I wish SmartyPants used named entities like `&mdash;`
instead of decimal-encoded entites like `&#8212;`.

***
